package solveraapps.animationtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

/**
 * Created by andreas on 09.03.2018.
 */

public class MyView extends View
{

    public Ball myBall = new Ball(200,200,40);

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint=new Paint();
        paint.setColor(Color.GREEN);
        drawBall(canvas,paint,myBall);
        Log.v("DEBUG_DRAW","in onDraw()");

    }

    public void refresh(){
        Log.v("DEBUG_DRAW","in refresh()");
        this.invalidate();
    }


    public void drawBall(Canvas canvas, Paint paint , Ball theBall){
        canvas.drawCircle(theBall.getX(), theBall.getY(), theBall.getSize(), paint);


    }

    public MyView(Context context)
    {
        super(context);
        setWillNotDraw(false);
    }

}